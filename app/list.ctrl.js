(function () {
    'use strict';

    angular.module('app').controller('ListCtrl', Ctrl);

    function Ctrl($http, modalService) {

        var vm = this;

        this.items = [];
        this.newItem = '';
        this.addNew = addNew;
        this.removeItem = removeItem;


        init();

        function init() {
            $http.get('api/tasks').then(function (result) {
                vm.items = result.data;
            });
        }

        function addNew() {

            var newItem = {
                title: this.newItem,
                done: false
            };

            $http.post('api/tasks', newItem).then(init);

            this.newItem = '';
        };

        function removeItem(id) {

            modalService.confirm()
                .then(function () {
                    return $http.delete('api/tasks/' + id);
                }).then(init);
        }
    }

})();


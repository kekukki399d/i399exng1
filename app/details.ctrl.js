(function () {
    'use strict';

    angular.module('app').controller('DetailsCtrl', Ctrl);

    function Ctrl($http, $routeParams, $location) {
        var vm = this;

        this.item = {};

        vm.back = back;

        $http.get('api/tasks/' + $routeParams.id).then(function(result) {
            vm.item = result.data;
        });

        function back() {
            $location.path('/list');
        }

    }

})();

